﻿
using System.Web.Mvc;
using System.Web.Routing;

namespace TP_Adivinator.Filters
{
    public class UserFilter : ActionFilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Session["Player"] == null)
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "Controller", "Game" }, { "Action", "Index" } });
        }
    }
}