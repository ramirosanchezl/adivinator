﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using TP_Adivinator.Filters;
using TP_Adivinator.Models;

namespace TP_Adivinator.Controllers
{
    public class GameController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            if(Session["Player"] != null)
            {
                return RedirectToAction("MatchLoad");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Login(string name) 
        {
            var game = Game.GetGame();
            var player = game.GetPlayer(name);
            Session["Player"] = player;
            var wonMatches = 0;
            Session["WonMatches"] = wonMatches;
            return RedirectToAction("MatchLoad");

        }
        [UserFilter]
        public ActionResult MatchLoad()
        {
            Player player = (Player)Session["Player"];
            Match match = new Match(player);
            match.SetRandom();
            HttpCookie wonMatchesNav = Request.Cookies["wonMatchesNav"];
            if (wonMatchesNav != null)
            {
                var WonMatchesNave = wonMatchesNav["NavRecord"].ToString();
                Session["NavRecord"] = Convert.ToInt32(WonMatchesNave);
            }
            else
            {
                int navRecord = 0;

                Session["NavRecord"] = navRecord;
            }
            Session["Match"] = match;
            return View("Play", match);
        }

        [UserFilter]
        public ActionResult Play()
        {
            Match match = (Match)Session["Match"];
            int numberOfPlayer = Convert.ToInt16(Request.Form["number"]);
            if (numberOfPlayer < 101 && numberOfPlayer > 0)
            {
                int result = match.Play(numberOfPlayer);
                if (result == 0)
                {
                    match.SetWin(match);
                    var wonMatches = (int)Session["WonMatches"];
                    wonMatches++;
                    Session["WonMatches"] = wonMatches;
                    ViewBag.Result = "¡Ganaste!";
                }
                if (result > 0)
                {
                    ViewBag.Result = "¡Numero mas bajo!";
                }
                if (result < 0)
                {
                    ViewBag.Result = "¡Numero mas alto!";
                }
                if (match.GetAttempts() == 0)
                {
                    ViewBag.Result = "¡Perdiste!";

                }
            }
            else
            {
                ViewBag.Result = "¡Numero fuera de rango!";
            }
            
            return View(match);
        }

        public ActionResult Ranking()
        {
            var game = Game.GetGame();
            string bestPlayer = null;
            var bestScore = 0;
            foreach (KeyValuePair<string, Player> player in game.players)
            {
                if(player.Value.wonMatches.Count() > bestScore)
                {
                    bestScore = player.Value.wonMatches.Count();
                    bestPlayer = player.Key;
                }
            }
            ViewBag.bestScore = bestScore;
            ViewBag.bestPlayer = bestPlayer;
            return View();
        }

        [UserFilter]
        [HttpGet]
        public ActionResult LogOut()
        {
            int navRecord = (int)Session["NavRecord"];
            int wonMatches = (int)Session["WonMatches"];
            if (wonMatches >  navRecord)
            {

                navRecord = wonMatches;
                
            }
            HttpCookie wonMatchesNav = new HttpCookie("wonMatchesNav");
            wonMatchesNav["NavRecord"] = Convert.ToString(navRecord);
            wonMatchesNav.Expires = new DateTime(2020, 12, 25);
            Response.Cookies.Add(wonMatchesNav);

            Session.Abandon();
            return View("Index");
        }
    }

}