﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP_Adivinator.Models
{
    public class Match
    {
        public Player player;
        private int randomNumber;
        private readonly int maxAttempts = 10;
        public List<int> attempts = new List<int>();
        public bool Won { get; set; }

        public Match(Player player)
        {
            this.player = player;
        }


        public void SetRandom () 
        {
            Random r = new Random();
            randomNumber = r.Next(minValue: 1, maxValue: 100);
        }

        public void SetWin(Match match) 
        {
            player.AddWin(match);
            Won = true;
        }

        public int GetAttempts()
        {
            return maxAttempts - this.attempts.Count();
        }
        public bool Validate(int numberOfUser) 
        {
            if(numberOfUser > 0 && numberOfUser < 101)
            {
                return true;
            }
            return false;
        }


        public int Play (int numberOfUser) 
        {
            if( Validate(numberOfUser))
            {
                Won = numberOfUser == this.randomNumber;
                if (!Won)
                {
                    this.attempts.Add(Convert.ToInt32(numberOfUser));
                }
            }
            return numberOfUser - this.randomNumber;
        }
    }
}