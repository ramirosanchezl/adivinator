﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP_Adivinator.Models
{
    public class Game
    {

        public Dictionary<string, Player> players = new Dictionary<string, Player>();
        private static Game game;


        static public Game GetGame()
        {
            if (game == null)
            {
                game = new Game();
            }
            return game;
        }

        public Player GetPlayer(string name)
        {
            Player player;
            if (!players.ContainsKey(name))
            {
                player = new Player(name);
                players.Add(name, player);
            }
            else
            {
                player = players[name];
            }

            return player;
        }

        public Match StartGame(String name)
        {
            return (StartGame(GetPlayer(name)));
        }

        public Match StartGame(Player player)
        {
            return new Match(player);
        }

        public void AddRecord(Match match)
        {
            try
            {
                players.Add(match.player.Name, match.player);
            }
            catch (ArgumentException)
            {
                string name = match.player.Name;
                // Player player = players.Values(name); obtener jug en lista
                players.Remove(name);
            }


        }
    }



}