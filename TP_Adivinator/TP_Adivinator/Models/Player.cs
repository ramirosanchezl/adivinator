﻿
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    namespace TP_Adivinator.Models
    {
        public class Player
        {
            
            public string Name { get; set; }


            public List<Match> wonMatches = new List<Match>();

            public Player(string name)
            {
                this.Name = name;
            }
           

            public void AddWin(Match match) 
            {
                wonMatches.Add(match);
            }
        }   
    }